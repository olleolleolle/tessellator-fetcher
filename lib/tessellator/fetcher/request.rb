require 'tessellator/fetcher/version'
require 'uri'
require 'openssl/better_defaults'
require 'net/https'

class Tessellator::Fetcher
  require 'tessellator/fetcher/response'

  class Request
    def self.handler_for_scheme(scheme)
      requester = self.constants.map(&method(:const_get)).find {|x| x.schemes.include?(scheme)}

      unless requester
        raise NotImplementedError, "No way to handle #{uri.scheme} URIs."
      end

      requester
    end
  end

  class BaseRequest
    class << self
      attr_accessor :config

      def call(config, method, url, parameters, options={})
        @config = config

        uri = Helpers.safe_uri(options[:url_override] || url)

        unless schemes.include?(uri.scheme)
          raise NotImplementedError, "#{self.name} cannot handle #{uri.scheme} URIs."
        end

        Response.new(*fetch(method, url, parameters, options))
      end

      def schemes
        const_get(:SCHEMES)
      end

      private
      def fetch(method, url, parameters, options)
        raise NotImplementedError
      end
    end
  end
end
