require 'heresy/string'
require 'uri'

class Tessellator::Fetcher
  require 'tessellator/fetcher/request'

  class Request::Internal < BaseRequest
    SCHEMES = %w[about]

    class << self
      def fetch(method, url, parameters, options={})
        uri  = Helpers.safe_uri(url)
        body = fetch_file(uri)

        data = {
          url:          options[:url_override] || url,
          version:      Fetcher.version,
          user_agent:   Fetcher.user_agent,
        }

        if body
          [body.format(data), {'content-type' => 'text/html'}, data[:url]]
        else
          request('GET', 'errors:404', {}, {url_override: url})
        end
      end

      def fetch_file(uri)
        filename = uri.opaque + '.html'

        return nil if filename.start_with?('.') || filename.start_with?('/')

        file_path = File.join(Fetcher.pages_path, uri.scheme, filename)

        return nil unless File.file?(file_path)

        open(file_path).read
      end
    end
  end
end
