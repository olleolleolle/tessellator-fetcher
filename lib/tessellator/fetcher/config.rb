module Tessellator
  class Fetcher
    class Config
      KEYS = [:version, :user_agent, :http_redirect_limit, :pages_path]

      attr_accessor *KEYS

      class ConfigError < StandardError
      end

      def initialize(hash={}, &block)
        self.version    = hash[:version] || Tessellator::Fetcher::VERSION
        self.user_agent = hash[:user_agent] || "TessellatorFetcher/v#{self.version}"
        self.http_redirect_limit = hash[:http_redirect_limit] || 10
        self.pages_path = hash[:pages_path] || ''

       instance_exec(self, &block) if block
      end

      def inspect
        options = KEYS.map {|k| "#{k}=#{send(k).inspect}" }
        "#<Tessellator::Fetcher::Config #{options.join(' ')}>"
      end
    end
  end
end
